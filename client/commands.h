#include <netdb.h>

#define MAXLEN 2048
#define BUFLEN 256
#define QUESTMAX 11
#define TOPICMAX 11

#define DEF_PORT "58021"
#define DEF_IP "127.0.0.1"
#define TOPICS_FOLDER "client/topics"
#define TOPICS_LIST "topics.FS"
#define QUESTION_LIST "questions.FS"
#define ANSWER_LIST "answers.FS"

#define MIN(a,b) ((a < b) ? a : b)

void parseArgs(int argc, char *argv[]);
void getCommand(char **command, int *size);
void parseInput(char *command, int size);

void TCPconnect();
void TCPclose();
int TCPread(char *command, int size);
int TCPwrite(char *str, int size);
int TCPgetFile(char *name, int size);
int TCPsendFile(char *name);

int UDPcommunication(char *command, int size, int write);

void f_REG(char *command, int size);
void f_LTP(char *command, int size);
void f_TS();
void f_TSelect();
void f_PTP(char *command, int size);
void f_LQU(char *command, int size);
void f_QGet(char *command, int size);
void f_QG(char *command, int size);
void f_QUS(char *command, int size);
void f_ANS(char *command, int size);
