O servidor e cliente têm os ficheiros de código armazenados nas diretorias 'server' e 'client' respetivamente

Tanto o servidor como o cliente devem ser executados diretamente da pasta onde foram compilados (a diretoria que contém o Makefile)

Tanto o servidor como o cliente utilizam a mesma estrutura de organização de diretorias:

topics       <-- diretoria principal (contém as diretorias referentes aos tópicos)
|-- topics.FS        <-- ficheiro que contém os tópicos e o ID do utilizador que propôs cada um
`-- topico1        <-- diretoria do tópico topico1
    |-- answers.FS        <-- ficheiro que contém todas as respostas de todas as perguntas
    |                         referentes ao tópico 'topico1' e o ID do utilizador que a submeteu 
    |-- questions.FS        <-- ficheiro que contém todas as perguntas referentes ao tópico 'topico1' e o userID
    |-- pergunta.answers        <-- ficheiro que contém o número de respostas para a pergunta 'pergunta'
    |-- pergunta.txt        <-- ficheiro (nome = título da pergunta) que contém a pergunta
    |-- pergunta.jpg        <-- ficheiro de imagem da pergunta
    |-- pergunta_NN.txt        <-- ficheiro (nome = título da pergunta correspondente e numero da resposta)
    |                              que contém a resposta à pergunta 'pergunta'
    `-- pergunta_NN.png        <-- ficheiro de imagem da resposta NN

Para visualizar a informação recebida pelo cliente devemos aceder à diretoria 'client/topics' e para o servidor
devemos aceder à diretoria 'server/topics'

O cliente aceita tanto caminhos relativos como absolutos para o envio de ficheiros. No caso dos caminhos relativos,
o working directory do cliente é o local onde este foi compilado (a diretoria que contém o Makefile), no entanto 
não é possível aceder a diretorias que contenham '.' no nome

Os ficheiros auxiliares para ajudar a testar o cliente estão na diretoria 'client'
