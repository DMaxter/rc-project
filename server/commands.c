#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include "commands.h"

// Better strtok
void *bestStrtok(char *str, char *del){
	static char *ptr;
	static char *len;
	static int i;

	if(str == NULL){
		ptr += i + 1;
		if(len == NULL || ptr == len + 1){
			len = NULL;
			return NULL;
		}
	}else{
		ptr = str;

		len = str + strlen(str);
	}

	i = strcspn(ptr, del);

	ptr[i] = '\0';

	return ptr;
}

// TCP Connections
void TCP(int fd){
	char *command = malloc(sizeof(char) * COMMAND_LEN);
	if(command == NULL){
		perror("malloc");
		exit(1);
	}

	// Get command
	TCPread(fd, command, COMMAND_LEN);

	parseInput(fd, command, COMMAND_LEN, 1);

	free(command);
}

// Receive size bytes from TCP connection
// Read until ' ' or '\n' are found
// or until size bytes were read
// store data in command
int TCPread(int fd, char *command, int size){
	int n, toRead = size;
	int i = 0;
	char *ptr = command;

	while((n = read(fd, ptr, 1))){
		if(n == -1){
			perror("read");
			exit(1);
		}

		toRead--;

		if(*ptr == '\n'){
			i = 1;

			break;
		}else if(*ptr == ' '){
			if(toRead == size - 1){
				i = 3;
			}else{
				i = 2;
			}

			break;
		}else if(toRead == 0){
			break;
		}

		ptr++;
	}

	*ptr = '\0';

	return i;
}

// Send size bytes of str to TCP connection
void TCPwrite(int fd, char *str, int size){
	int n, toWrite = size;
	char *ptr = str;

	while(toWrite){
		n = write(fd, str, toWrite);
		if(n == -1){
			perror("write");
			exit(1);
		}

		toWrite -= n;
		ptr++;
	}
}

// Discard all information
void TCPdiscard(int fd, char *command, int size){
	int n;
	while((n = read(fd, command, size))){
		if(n == -1){
			perror("read");
			exit(1);
		}

		if(command[n-1] == '\n' && n < size){

			return;
		}
	}
}

// Receive file with size bytes from TCP connection
// and store the content as "name" file
void TCPgetFile(int fd, char *name, int size){
	FILE *fp = fopen(name, "wb");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	char buffer[BUFFER_LEN];

	int n, toRead = size;
	while(toRead){
		n = read(fd, buffer, MIN(toRead, BUFFER_LEN));
		if(n == -1){
			perror("read");
			exit(1);
		}

		fwrite(buffer, sizeof(char), n, fp);
		toRead -= n;
	}

	fclose(fp);
}

// Send file "name" to TCP connection
void TCPsendFile(int fd, char *name){
	FILE *fp = fopen(name, "rb");
	if(fp == NULL){
		write(fd, "ERR\n", 4);
		exit(1);
	}

	char *ptr, buffer[BUFFER_LEN];
	int toWrite, n;
	while(feof(fp) == 0){
		toWrite = fread(buffer, sizeof(char), BUFFER_LEN, fp);
		ptr = buffer;

		while(toWrite){
			n = write(fd, ptr, toWrite);
			if(n == -1){
				perror("write");
				exit(1);
			}

			toWrite -= n;
			ptr += n;
		}
	}

	fclose(fp);
}

// UDP Connections
void UDP(int fd){
	struct sockaddr_in *addr;
	socklen_t addrlen = sizeof(addr);
	char *command = malloc(sizeof(char) * COMMAND_LEN);
	if(command == NULL){
		perror("malloc");
		exit(1);
	}

	int n;
	int commandLen = COMMAND_LEN;

	n = recvfrom(fd, command, commandLen, 0, (struct sockaddr *) &addr, &addrlen);
	if(n == -1){
		perror("recvfrom");
		exit(1);
	}

	// Remove '\n'
	command[n-1] = '\0';

	char ip[INET_ADDRSTRLEN];
	struct sockaddr_in* pAddr = (struct sockaddr_in*)&addr;
	struct in_addr ipAddr = pAddr->sin_addr;

	inet_ntop(AF_INET, &ipAddr, &ip, INET_ADDRSTRLEN);
	printf("%s: ", ip);

	parseInput(fd, command, COMMAND_LEN, 0);

	commandLen = strlen(command);
	command[commandLen++] = '\n';

	n = sendto(fd, command, commandLen, 0, (struct sockaddr *) &addr, addrlen);
	if(n == -1){
		perror("sendto");
		exit(1);
	}

	free(command);
}

// Choose which command to execute
void parseInput(int fd, char *command, int size, int TCP){
	char *token = bestStrtok(command, " ");

	if(strcmp(token, "REG") == 0){
		printf("Registering user\n");
		f_RGR(command, size);
	}else if(strcmp(token, "LTP") == 0){
		printf("Listing topics\n");
		f_LTR(command, size);
	}else if(strcmp(token, "PTP") == 0){
		printf("Proposing a topic\n");
		f_PTR(command, size);
	}else if(strcmp(token, "LQU") == 0){
		printf("Listing questions\n");
		f_LQR(command, size);
		// UNTIL HERE IT WORKS
	}else if(strcmp(token, "GQU") == 0){
		printf("Getting a question\n");
		f_QGR(fd, command, size);
	}else if(strcmp(token, "QUS") == 0){
		printf("Asking a question\n");
		f_QUR(fd, command, size);
	}else if(strcmp(token, "ANS") == 0){
		printf("Answering a question\n");
		f_ANR(fd, command, size);
	}else{
		printf("Doing random stuff\n");
		if(TCP){
			int n = snprintf(command, size, "ERR\n");
			TCPwrite(fd, command, n);
		}

		snprintf(command, size, "ERR");
	}
}

void f_RGR(char *command, int size){
	char *token = bestStrtok(NULL, " ");

	if(token == NULL){
		snprintf(command, size, "RGR NOK");
		return;
	}

	int user = strtol(token, NULL, 10);

	command[0] = '\0';

	if(validateUser(user)){
		snprintf(command, size, "RGR OK");
	}else{
		snprintf(command, size, "RGR NOK");
	}
}

void f_LTR(char *command, int size){
	command[0] = '\0';

	if(bestStrtok(NULL, " ") != NULL){
		snprintf(command, size, "ERR");
		return;
	}

	// Check for file containing topics
	if(access(TOPICS_LIST, F_OK)){
		snprintf(command, size, "LTR 0");
		return;
	}

	FILE *fp = fopen(TOPICS_LIST, "r");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	// Save topics
	char *temp = malloc(sizeof(char) * COMMAND_LEN);
	if(temp == NULL){
		perror("malloc");
		exit(1);
	}

	temp[0] = '\0';
	char *ptr = temp;

	int n = 0, chars = 0;
	int tmpSize = COMMAND_LEN;
	// Only 16 chars needed, but more for security reasons
	char buffer[20];

	// Get topic and user one by one
	while(fgets(buffer, 20, fp) != NULL){
		strtok(buffer, "\n");

		chars = snprintf(ptr, tmpSize, " %s", buffer);

		tmpSize -= chars;
		ptr += chars;

		n++;
	}

	if(n == 0){
		snprintf(command, size, "LTR 0");
	}else{
		snprintf(command, size, "LTR %d%s", n, temp);
	}

	free(temp);
	fclose(fp);
}

void f_PTR(char *command, int size){
	int N = 0;

	char *token = bestStrtok(NULL, " ");
	if(token == NULL){
		snprintf(command, size, "PTR NOK");
		return;
	}
	int user = strtol(token, NULL, 10);

	if(!validateUser(user)){
		snprintf(command, size, "PTR NOK");
		return;
	}

	// Get topic
	token = bestStrtok(NULL, " ");

	if(strlen(token) > 10 || token == NULL){
		snprintf(command, size, "PTR NOK");
		return;
	}

	DIR *dir = opendir(".");
	if (dir == NULL){
		perror("opendir");
		exit(1);
	}

	struct dirent *dirptr;

	// Check for duplicate
	while ((dirptr = readdir(dir)) != NULL){
		// Filters current and parent directory
		if (strcmp(dirptr->d_name, ".") != 0 && strcmp(dirptr->d_name, "..") != 0){
			N++;

			if (strcmp(dirptr->d_name, token) == 0){
				snprintf(command, size, "PTR DUP");
				return;
			}
		}
	}

	closedir(dir);

	if(N == MAX_TOPICS){
		snprintf(command, size, "PTR FUL");
		return;
	}

	// Create topic folder
	if(mkdir(token, 0777) == -1){
		perror("mkdir");
		exit(1);
	}

	FILE *fp = fopen(TOPICS_LIST, "a");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	fprintf(fp, "%s:%d\n", token, user);
	fclose(fp);

	snprintf(command, size, "PTR OK");
}

void f_LQR(char *command, int size){
	// Get topic
	char *token = bestStrtok(NULL, " ");
	if(token == NULL){
		snprintf(command, size, "ERR");
		return;
	}

	if(chdir(token) != 0){
		snprintf(command, size, "ERR");
		return;
	}

	// Check if there are questions
	if(access(QUESTION_LIST, F_OK)){
		snprintf(command, size, "LQR 0");
		return;
	}

	FILE *fp = fopen(QUESTION_LIST, "r+");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	char *temp = malloc(sizeof(char) * COMMAND_LEN);
	if(temp == NULL){
		perror("malloc");
		exit(1);
	}

	temp[0] = '\0';
	char *ptr = temp;

	int n = 0, chars = 0;
	int tmpSize = COMMAND_LEN;
	// Only 22 needed
	char buffer[25];

	// Get questions one by one
	while(fgets(buffer, 25, fp) != NULL) {
		strtok(buffer, "\n");

		chars = snprintf(ptr, tmpSize, " %s", buffer);

		ptr += chars;
		tmpSize -= chars;

		strtok(buffer, ":");
		strcat(buffer, ".answers");

		int numAns;

		FILE *aNum = fopen(buffer, "r");
		if(aNum == NULL){
			perror("fopen");
			exit(1);
		}

		fscanf(aNum, "%d", &numAns);

		fclose(aNum);

		chars = snprintf(ptr, tmpSize, ":%d", numAns);

		ptr += chars;
		tmpSize -= chars;

		n++;
	}

	fclose(fp);

	snprintf(command, size, "LQR %d%s", n, temp);

	free(temp);
}

void f_QGR(int fd, char *command, int size){
	char buffer[BUFFER_LEN], file[FILE_MAX], question[QUEST_MAX];
	int userID, exists = 0, answers, n, curAns;
	char ext[4], *ptr;

	struct dirent *dirptr;

	// Get topic
	if(TCPread(fd, command, size) != 2){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QGR ERR\n");
		TCPwrite(fd, command, size);
		return;
	}

	if(chdir(command) != 0){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QGR EOF\n");
		TCPwrite(fd, command, n);
		return;
	}

	FILE *fp = fopen(QUESTION_LIST, "r");
	if(fp == NULL){
		TCPdiscard(fd, command, size);

		int n = snprintf(command, size, "ERR\n");
		TCPwrite(fd, command, n);
		return;
	}

	// Get question
	if(TCPread(fd, command, size) != 1){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QGR ERR\n");
		TCPwrite(fd, command, size);
		return;
	}

	snprintf(question, QUEST_MAX, "%s", command);

	// Get user ID
	char *questions;
	while(fgets(buffer, BUFFER_LEN, fp) != NULL) {
		questions = strtok(buffer, ":");
		if(strcmp(questions, question) == 0){
			exists = 1;
			questions = strtok(NULL, ":");
			userID = strtol(questions, NULL, 10);

			break;
		}
	}

	fclose(fp);

	if(exists == 0){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QGR EOF\n");
		TCPwrite(fd, command, n);
		return;
	}

	// Check size
	snprintf(buffer, BUFFER_LEN, "%s.txt", question);
	fp = fopen(buffer, "r");
	if(fp == NULL){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ERR\n");
		TCPwrite(fd, command, n);
		return;
	}

	fseek(fp, 0, SEEK_END);

	// Send QGR <user> <size> <data>
	n = snprintf(command, size, "QGR %d %d ", userID, (int) ftell(fp));
	fclose(fp);

	TCPwrite(fd, command, n);
	TCPsendFile(fd, buffer);

	// Check for image
	exists = 0;
	snprintf(buffer, BUFFER_LEN, "%s.", question);

	DIR *dir = opendir(".");
	if (dir){
		while ((dirptr = readdir(dir)) != NULL){
			// Filters current and parent directory
			if (strcmp(dirptr->d_name, ".") != 0 && strcmp(dirptr->d_name, "..") != 0 &&
				strcmp(strtok(dirptr->d_name, "."), question)  == 0 &&
				strcmp(ptr = strtok(NULL, "."), "txt") != 0 && strcmp(ptr, "answers") != 0){
					snprintf(ext, 4, "%s", ptr);
					exists = 1;
					break;
			}
		}

		closedir(dir);
	}else{
		n = snprintf(command, size, "ERR\n");

		TCPdiscard(fd, command, size);
		TCPwrite(fd, command, size);
	}

	// Check extension, size and send data
	if(exists){
		snprintf(buffer, BUFFER_LEN, "%s.%s", question, ext);
		fp = fopen(buffer, "rb");
		if(fp == NULL){
			perror("fopen");
			exit(1);
		}

		fseek(fp, 0, SEEK_END);

		n = snprintf(command, size, " %d %s %d ", exists, ext, (int) ftell(fp));
		fclose(fp);

		TCPwrite(fd, command, n);
		TCPsendFile(fd, buffer);
	}else{
		n = snprintf(command, size, " %d", exists);

		TCPwrite(fd, command, n);
	}

	// Check number of answers
	snprintf(buffer, BUFFER_LEN, "%s.answers", question);
	fp = fopen(buffer, "r");

	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	fscanf(fp, "%d", &answers);
	fclose(fp);

	if(answers == 0){
		n = snprintf(command, size, " %d\n", answers);
		TCPwrite(fd, command, n);
		return;
	}else if(answers > 10){
		curAns = answers - 9;
		n = snprintf(command, size, " 10");
	}else{
		curAns = 1;
		n = snprintf(command, size, " %d", answers);
	}

	TCPwrite(fd, command, n);

	// Send answers
	while(curAns <= answers) {
		exists = 0;

		fp = fopen(ANSWER_LIST, "r");
		if(fp == NULL){
			perror("fopen");
			exit(1);
		}

		snprintf(file, FILE_MAX, "%s_%02d", question, curAns);

		while(fgets(buffer, BUFFER_LEN, fp) != NULL) {
			if(strcmp(strtok(buffer, ":"), file) == 0){
				userID = strtol(strtok(NULL, ":"), NULL, 10);
				break;
			}
		}

		fclose(fp);

		snprintf(file, BUFFER_LEN, "%s_%02d.txt", question, curAns);

		fp = fopen(file, "r");
		if(fp == NULL){
			perror("fopen");
			exit(1);
		}

		fseek(fp, 0, SEEK_END);

		n = snprintf(command, size, " %d %d %d ", curAns, userID, (int) ftell(fp));
		fclose(fp);

		TCPwrite(fd, command, n);
		TCPsendFile(fd, file);

		snprintf(file, FILE_MAX, "%s_%02d", question, curAns);

		// Check for image
		exists = 0;

		dir = opendir(".");
		if(dir){
			while ((dirptr = readdir(dir)) != NULL){
				// Filter current and parent directory
				if (strcmp(dirptr->d_name, ".") != 0 && strcmp(dirptr->d_name, "..") != 0 &&
					strcmp(strtok(dirptr->d_name, "."), file) == 0 &&
					strcmp(ptr = strtok(NULL, "."), "txt") != 0 && strcmp(ptr, "answers") != 0){
						snprintf(ext, 4, "%s", ptr);
						exists = 1;
						break;
				}
			}

			closedir(dir);
		}else{
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "ERR\n");
			TCPwrite(fd, command, size);
		}

		if(exists){
			snprintf(file, BUFFER_LEN, "%s_%02d.%s", question, curAns, ext);

			fp = fopen(file, "rb");
			if(fp == NULL){
				perror("fopen");
				exit(1);
			}

			fseek(fp, 0, SEEK_END);

			n = snprintf(command, size, " %d %s %d ", exists, ext, (int) ftell(fp));
			fclose(fp);

			TCPwrite(fd, command, n);
			TCPsendFile(fd, file);
		}else{
			TCPwrite(fd, " 0", 2);
		}

		if(curAns == answers){
			TCPwrite(fd, "\n", 1);
		}

		curAns++;
	}
}

void f_QUR(int fd, char *command, int size){
	int n;
	// Get userID
	if(TCPread(fd, command, size) != 2){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR NOK\n");
		TCPwrite(fd, command, size);
		return;
	}

	int userID = strtol(command, NULL, 10);

	if(!validateUser(userID)){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR NOK\n");
		TCPwrite(fd, command, n);
		return;
	}

	// Get topic
	if(TCPread(fd, command, size) != 2){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR NOK\n");
		TCPwrite(fd, command, size);
	}

	if(chdir(command) != 0){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR NOK\n");
		TCPwrite(fd, command, n);
		return;
	}

	// Get question title
	if(TCPread(fd, command, size) != 2){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR NOK\n");
		TCPwrite(fd, command, size);
		return;
	}

	char buffer[BUFFER_LEN];

	FILE *fp = fopen(QUESTION_LIST, "a+");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	// Check for duplicates
	int N = 0;
	while(fgets(buffer, BUFFER_LEN, fp) != NULL){
		N++;
		if(strcmp(command, strtok(buffer, ":")) == 0){
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "QUR DUP\n");
			TCPwrite(fd, command, n);
			return;
		}
	}

	if(N == MAX_QUESTIONS){
		fclose(fp);

		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR FUL\n");
		TCPwrite(fd, command, n);
		return;
	}

	// Add entry to question list
	fprintf(fp, "%s:%d\n", command, userID);
	fclose(fp);

	// Create answer file
	snprintf(buffer, BUFFER_LEN, "%s.answers", command);
	fp = fopen(buffer, "w");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	fprintf(fp, "0");
	fclose(fp);

	// Get question file
	char *ptr = buffer;
	n = snprintf(buffer, BUFFER_LEN, "%s.txt", command);
	ptr += n;

	if(TCPread(fd, command, size) != 2){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR NOK\n");
		TCPwrite(fd, command, size);
		return;
	}

	TCPgetFile(fd, buffer, strtol(command, NULL, 10));

	if(TCPread(fd, command, size) != 3){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "QUR NOK\n");
		TCPwrite(fd, command, size);
	}

	TCPread(fd, command, size);

	if(strtol(command, NULL, 10) == 1){
		// To overwrite extension
		ptr -= 3;

		if(TCPread(fd, command, size) != 2 || strlen(command) != 3){
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "QUR NOK\n");
			TCPwrite(fd, command, size);
			return;
		}

		snprintf(ptr, BUFFER_LEN - n, "%s", command);

		if(TCPread(fd, command, size) != 2){
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "QUR NOK\n");
			TCPwrite(fd, command, size);
			return;
		}

		TCPgetFile(fd, buffer, strtol(command, NULL, 10));

		if(TCPread(fd, command, size) != 1 || strlen(command) != 0){
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "QUR NOK\n");
			TCPwrite(fd, command, size);
			return;
		}
	}

	n = snprintf(command, size, "QUR OK\n");
	TCPwrite(fd, command, n);
}

void f_ANR(int fd, char *command, int size){
	char buffer[BUFFER_LEN];
	int N = 0, n, qExists = 0;

	if(TCPread(fd, command, size) != 2){ //aUserID
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR NOK\n");
		TCPwrite(fd, command, size);
		return;
	}

	int userID = strtol(command, NULL, 10);

	if(validateUser(userID) == 0){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR NOK\n");
		TCPwrite(fd, command, n);
		return;
	}

	if(TCPread(fd, command, size) != 2){ //topic
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR NOK\n");
		TCPwrite(fd, command, size);
		return;
	}

	if(chdir(command) != 0){
		TCPdiscard(fd, command, size);

		strcpy(command, "ERR\n");
		return;
	}

	// Check if question exists
	if(TCPread(fd, command, size) != 2){ //question
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR NOK\n");
		TCPwrite(fd, command, size);
		return;
	}

	FILE *fp = fopen(QUESTION_LIST, "r+");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	while(fgets(buffer, BUFFER_LEN, fp) != NULL){
		if(strcmp(command, strtok(buffer, ":")) == 0){
			qExists = 1;
			break;
		}
	}
	fclose(fp);

	if(qExists == 0){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR NOK\n");
		TCPwrite(fd, command, n);
		return;
	}

	// Check if list is full
	snprintf(buffer, BUFFER_LEN, "%s.answers", command);

	fp = fopen(buffer, "r+");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	fscanf(fp, "%d", &N);

	if(N == MAX_ANSWERS){
		fclose(fp);

		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR FUL\n");
		TCPwrite(fd, command, n);
		return;
	}

	N++;

	rewind(fp);
	fprintf(fp, "%d", N);
	fclose(fp);

	fp = fopen(ANSWER_LIST, "a+");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	fprintf(fp, "%s_%02d:%d", command, N, userID);
	fclose(fp);

	// Create answer file
	char *ptr = buffer;
	n = snprintf(buffer, BUFFER_LEN, "%s_%02d.txt", command, N);
	ptr += n;

	if(TCPread(fd, command, size) != 2){ //asize
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR NOK\n");
		TCPwrite(fd, command, n);
		return;
	}

	TCPgetFile(fd, buffer, strtol(command, NULL, 10)); //creates file with adata

	if(TCPread(fd, command, size) != 3){
		TCPdiscard(fd, command, size);

		n = snprintf(command, size, "ANR NOK\n");
		TCPwrite(fd, command, n);
		return;
	}

	TCPread(fd, command, size); //aIMG

	if(strtol(command, NULL, 10) == 1){
		// To overwrite extension
		ptr -= 3;

		if(TCPread(fd, command, size) != 2){ //iext
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "ANR NOK\n");
			TCPwrite(fd, command, n);
			return;
		}

		snprintf(ptr, BUFFER_LEN - n, "%s", command);
		if(TCPread(fd, command, size) != 2){ //isize
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "ANR NOK\n");
			TCPwrite(fd, command, n);
			return;
		}

		TCPgetFile(fd, buffer, strtol(command, NULL, 10)); //creates file with idata

		if(TCPread(fd, command, size) != 1 || strlen(command) != 0){
			TCPdiscard(fd, command, size);

			n = snprintf(command, size, "ANR NOK\n");
			TCPwrite(fd, command, n);
			return;
		}
	}

	n = snprintf(command, size, "ANR OK\n");
	TCPwrite(fd, command, n);
}

// Check user id
int validateUser(int user){
	int nDigits = 0;

	while(user != 0){
		user /= 10;
		++nDigits;
	}

	return (nDigits == 5);
}
