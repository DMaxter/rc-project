#include <netinet/in.h>

#define PORT "58021"

#define COMMAND_LEN 2048
#define BUFFER_LEN 256
#define FILE_MAX 15
#define QUEST_MAX 11
#define MAX_TOPICS 99
#define MAX_QUESTIONS 99
#define MAX_ANSWERS 99

#define TOPICS_FOLDER "server/topics"
#define TOPICS_LIST "topics.FS"
#define QUESTION_LIST "questions.FS"
#define ANSWER_LIST "answers.FS"

#define MIN(a,b) ((a < b) ? a : b)
#define MAX(a,b) ((a > b) ? a : b)

void TCP(int fd);
int TCPread(int fd, char *command, int size);
void TCPwrite(int fd, char *str, int size);
void TCPdiscard(int fd, char *command, int size);
void TCPgetFile(int fd, char *name, int size);
void TCPsendFile(int fd, char *name);
void UDP(int fd);
void parseInput(int fd, char *command, int size, int TCP);
void f_RGR(char *command, int size);
void f_LTR(char *command, int size);
void f_PTR(char *command, int size);
void f_LQR(char *command, int size);
void f_QGR(int fd, char *command, int size);
void f_QUR(int fd, char *command, int size);
void f_ANR(int fd, char *command, int size);
int validateUser(int user);
