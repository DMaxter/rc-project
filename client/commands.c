#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <netdb.h>
#include <dirent.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include "commands.h"

int TCPfd;
extern char *FSIP, *FSPort;
extern int UDPfd;
extern struct addrinfo *resTCP, *resUDP, tcpSock;

int userID = 0;
char topic[TOPICMAX];
char question[QUESTMAX];

// Parse the arguments given to the client
void parseArgs(int argc, char *argv[]){
	FSIP = malloc(sizeof(char) * 16);
	if(FSIP == NULL){
		perror("malloc");
		exit(1);
	}

	FSPort = malloc(sizeof(char) * 10);

	// Set defaults
	strcpy(FSIP, DEF_IP);
	strcpy(FSPort, DEF_PORT);

	int opt;

	while((opt = getopt(argc, argv, ":p:n:")) != -1){
		switch(opt){
			case 'n': free(FSIP);
					  FSIP = optarg;
					  break;
			case 'p': free(FSPort);
					  FSPort = optarg;
					  break;
			default : printf("Invalid option '-%c'\n", (char) opt);
		}
	}
}

// Better strtok
void *bestStrtok(char *str, char *del){
	static char *ptr;
	static char *len;
	static int i;

	if(str == NULL){
		ptr += i + 1;
		if(len == NULL || ptr == len + 1){
			len = NULL;
			return NULL;
		}
	}else{
		ptr = str;

		len = str + strlen(str);
	}

	i = strcspn(ptr, del);

	ptr[i] = '\0';

	return ptr;
}

// Get input from user
void getCommand(char **command, int *size){
	if(*command == NULL){
		*command = malloc(sizeof(char) * MAXLEN);
		if(*command == NULL){
			perror("malloc");
			exit(1);
		}

		*size = MAXLEN;
	}

	char buffer[BUFLEN];
	int n, chars = 0;

	char *ptr = *command;

	while((n = read(0, buffer, BUFLEN))){
		if(n == -1){
			perror("read");
			exit(1);
		}

		chars += n;

		// Check if fits in current memory
		if(chars >= *size){
			*size += MAXLEN;
			*command = realloc(*command, *size);
			if(*command == NULL){
				perror("realloc");
				exit(1);
			}
		}

		// Add to command
		n = snprintf(ptr, *size - chars + n, "%s", buffer);
		ptr += n;

		// Exit on message smaller
		if(n < BUFLEN || (n == BUFLEN && buffer[BUFLEN-1] == '\n')){
			break;
		}
	}

	// Remove '\n'
	(*command)[chars-1] = '\0';
}

// Choose which command to execute
void parseInput(char *command, int size){
	char *token = strtok(command, " ");

	if(token == NULL){
		return;
	}else if(strcmp(token,"register") == 0 || strcmp(token, "reg") == 0){
		f_REG(command, size);
	}else if(strcmp(token, "topic_list") == 0 || strcmp(token, "tl") == 0){
		f_LTP(command, size);
	}else if(strcmp(token, "topic_select") == 0){
		f_TSelect();
	}else if(strcmp(token, "ts") == 0){
		f_TS();
	}else if(strcmp(token, "topic_propose") == 0 || strcmp(token, "tp") == 0){
		f_PTP(command, size);
	}else if(strcmp(token, "question_list") == 0 || strcmp(token, "ql") == 0){
		f_LQU(command, size);
	}else if(strcmp(token, "question_get") == 0){
		f_QGet(command, size);
	}else if(strcmp(token, "qg") == 0){
		f_QG(command, size);
	}else if (strcmp(command, "question_submit") == 0 || strcmp(command, "qs") == 0) {
		f_QUS(command, size);
	}else if (strcmp(command, "answer_submit") == 0 || strcmp(command, "as") == 0) {
		f_ANS(command, size);
	}else if(strcmp(token, "exit") == 0){
		freeaddrinfo(resTCP);
		freeaddrinfo(resUDP);
		close(TCPfd);
		close(UDPfd);

		exit(0);
	}

	else{
	  printf("Invalid command\n");
	}
}

// Establish connection to server
void TCPconnect(){
	int err;
	if((err = getaddrinfo(FSIP, FSPort, &tcpSock, &resTCP)) != 0){
		fprintf(stderr, "%s", gai_strerror(err));
		exit(1);
	}

	TCPfd = socket(resTCP->ai_family, resTCP->ai_socktype, resTCP->ai_protocol);
	if(TCPfd == -1){
		perror(NULL);
		exit(1);
	}
	if(connect(TCPfd, (struct sockaddr *) resTCP->ai_addr, resTCP->ai_addrlen) != 0){
		perror("connect");
		exit(1);
	}
}

// Close TCP connection
void TCPclose(){
	close(TCPfd);
}

// Receive size bytes from TCP connection
// Read until ' ' or '\n' are found
// or until size bytes were read
// store data in command
int TCPread(char *command, int size){
	int n, toRead = size;
	int i = 0;
	char *ptr = command;

	while((n = read(TCPfd, ptr, 1))){
		if(n == -1){
			perror("read");
			exit(1);
		}

		toRead--;

		if(*ptr == '\n'){
			i = 1;

			break;
		}else if(*ptr == ' '){
			if(toRead == size - 1){
				i = 3;
			}else{
				i = 2;
			}

			break;
		}else if(toRead == 0){
			break;
		}

		ptr++;
	}

	*ptr = '\0';

	return i;
}

// Send size bytes of str to TCP connection
int TCPwrite(char *str, int size){
	int n, toWrite = size;
	char *ptr = str;

	while(toWrite){
		n = write(TCPfd, ptr, toWrite);
		if(n == -1){
			perror("write");
			exit(1);
		}

		toWrite -= n;
		ptr += n;
	}

	return 0;
}

// Receive file with size bytes from TCP connection
// and store the content as "name" file
int TCPgetFile(char *name, int size){
	FILE *fp = fopen(name, "wb");
	if(fp == NULL){
		perror("fopen");
		exit(1);
	}

	char buffer[BUFLEN];

	int n, toRead = size;
	while(toRead){
		n = read(TCPfd, buffer, MIN(toRead, BUFLEN));
		if(n == -1){
			perror("read");
			exit(1);
		}

		fwrite(buffer, sizeof(char), n, fp);
		toRead -= n;
	}

	fclose(fp);

	return 0;
}

// Send file "name" to TCP connection
int TCPsendFile(char *name){
	FILE *fp = fopen(name, "rb");
	if(fp == NULL){
		if(errno == ENOENT){
			printf("Couldn't find file\n");
			return -1;
		}

		perror("fopen");
		exit(1);
	}

	char *ptr, buffer[BUFLEN];
	int toWrite, n;
	while(feof(fp) == 0){
		toWrite = fread(buffer, sizeof(char), BUFLEN, fp);
		ptr = buffer;

		while(toWrite){
			n = write(TCPfd, ptr, toWrite);
			if(n == -1){
				perror("write");
				exit(1);
			}

			toWrite -= n;
			ptr += n;
		}
	}

	fclose(fp);

	return 0;
}

// UDP timeout connection
int UDPcommunication(char *command, int size, int write){
	int tries = 5;
	int n;
	struct timeval time;
	struct sockaddr_in addr;
	socklen_t addrlen = sizeof(addr);

	time.tv_sec = 5;
	time.tv_usec = 0;

	while(tries--){
		if(setsockopt(UDPfd, SOL_SOCKET, SO_RCVTIMEO, &time, sizeof(time)) == -1)	{
			perror("setsockopt");
			exit(1);
		}

		if(sendto(UDPfd, command, write, 0, resUDP->ai_addr, resUDP->ai_addrlen) == -1){
			perror("sendto");
			exit(1);
		}


		n = recvfrom(UDPfd, command, size, 0, (struct sockaddr*) &addr, &addrlen);
		if(n == -1){
			if(errno == ETIMEDOUT || errno == EAGAIN){
				printf("Resending message\n");
				continue;
			}

			perror("recvfrom");
			exit(1);
		}

		command[n - 1] = '\0';

		return 0;
	}

	printf("Couldn't contact server\n");
	return -1;
}

void f_REG(char *command, int size){
	char *token =  strtok(NULL, " ");

	if(token == NULL){
		printf("User ID number needed\n");
		return;
	}

	userID = strtol(token, NULL, 10);

	int n = snprintf(command, size, "REG %d\n", userID);

	if(UDPcommunication(command, size, n) == -1){
		return;
	}

	token = bestStrtok(command, " ");
	if(strcmp(token, "ERR") == 0){
		printf("An error occurred on the server\n");
	}else if(strcmp(token, "RGR") == 0){
		if(strcmp(bestStrtok(NULL, " "), "OK") == 0){
			printf("User registered successfully\n");
		}else{
			printf("Invalid user\n");
			userID = 0;
		}
	}else{
		printf("Unknown message received from server\n");
	}
}

void f_LTP(char *command, int size){
	if(strtok(NULL, " ") != NULL){
		printf("List topics takes no additional argument\n");
		return;
	}

	int n = snprintf(command, size, "LTP\n");

	if(UDPcommunication(command, size, n) == -1){
		return;
	}

	char buffer[BUFLEN];
	char *token = bestStrtok(command, " ");

	if(strcmp(token, "ERR") == 0){
		printf("An error occurred on the server\n");
	}else if(strcmp(token, "LTR") == 0){
		token = bestStrtok(NULL, " ");
		int topics = strtol(token, NULL, 10);

		snprintf(buffer, BUFLEN, "%s/%s", TOPICS_FOLDER, TOPICS_LIST);

		// Always overwrite file
		FILE *fp = fopen(buffer, "w");
		if(fp == NULL){
			perror("fopen");
			exit(1);
		}

		if(topics == 0){
			printf("No topics available\n");
			return;
		}

		int i = 1;

		printf("TOPICS:\n");

		char *ptr;
		while(i <= topics){
			token = bestStrtok(NULL, " ");
			fprintf(fp, "%s\n", token);

			int colon = strcspn(token, ":");
			token[colon] = '\0';

			ptr = &(token[colon+1]);

			printf("%2d - %s by %s\n", i, token, ptr);
			i++;
		}

		fclose(fp);
	}else{
		printf("Unknown message received from server\n");
	}
}

void f_TS(){
	char buffer[BUFLEN];

	snprintf(buffer, BUFLEN, "%s/%s", TOPICS_FOLDER, TOPICS_LIST);

	if(access(buffer, F_OK)){
		printf("No topics available\nPropose one or check for a list using 'tl'\n");
		return;
	}

	FILE *fp = fopen(buffer, "r");
	if(fp == NULL){
		if(errno == ENOENT){
			printf("Couldn't find file\n");
			return;
		}

		perror("fopen");
		exit(1);
	}

	char *token = strtok(NULL, " ");

	if(token == NULL){
		printf("Topic number needed\n");

		return;
	}

	int i = strtol(token, NULL, 10);

	if(i == 0){
		printf("Unknown topic\n");

		return;
	}

	while(fgets(buffer, BUFLEN, fp) != NULL){
		i--;

		if(i == 0){
			snprintf(topic, TOPICMAX, "%s", strtok(buffer, ":"));

			printf("Selected topic %s\n", topic);

			// Check for 'topics' folder
			snprintf(buffer, BUFLEN, "%s/%s", TOPICS_FOLDER, topic);

			DIR *dir = opendir(buffer);

			if(!dir){
				if(errno == ENOENT){
					if(mkdir(buffer, 0777) != 0){
						perror("mkdir");
						exit(1);
					}
				}else{
					perror("opendir");
					exit(1);
				}
			}

			closedir(dir);

			return;
		}
	}

	printf("Unknown topic\nPropose a topic or get a list with 'tl'\n");
	topic[0] = '\0';
}

void f_TSelect(){
	char buffer[BUFLEN];

	snprintf(buffer, BUFLEN, "%s/%s", TOPICS_FOLDER, TOPICS_LIST);

	if(access(buffer, F_OK)){
		printf("No topics available\nPropose one or check for a list using 'tl'\n");

		return;
	}

	FILE *fp = fopen(buffer, "r");
	if(fp == NULL){
		if(errno == ENOENT){
			printf("Couldn't find file\n");
			return;
		}

		perror("fopen");
		exit(1);
	}

	char *token, *inTopic = strtok(NULL, " ");
	if(inTopic == NULL){
		printf("Topic name needed\n");

		return;
	}

	while(fgets(buffer, BUFLEN, fp) != NULL){
		if(strcmp((token = strtok(buffer, ":")), inTopic) == 0){
			snprintf(topic, TOPICMAX, "%s", token);

			printf("Selected topic %s\n", topic);

			// Check for 'topics' folder
			snprintf(buffer, BUFLEN, "%s/%s", TOPICS_FOLDER, topic);

			DIR *dir = opendir(buffer);

			if(!dir){
				if(errno == ENOENT){
					if(mkdir(buffer, 0777) != 0){
						perror("mkdir");
						exit(1);
					}
				}else{
					perror("opendir");
					exit(1);
				}
			}

			closedir(dir);

			return;
		}
	}

	printf("Unknown topic\n");
}

void f_PTP(char *command, int size){
	char *token = strtok(NULL, " ");

	if(token == NULL){
		printf("Topic name needed\n");
		return;
	}else if(userID == 0){
		printf("Register user first\n");
		return;
	}else if(strlen(token) > 10){
		printf("Topic must be at most 10 characters long\n");
		return;
	}

	snprintf(topic, TOPICMAX, "%s", token);

	int n = snprintf(command, size, "PTP %d %s\n", userID, topic);

	if(UDPcommunication(command, size, n) == -1){
		return;
	}

	token = bestStrtok(command, " ");

	if(strcmp(token, "ERR") == 0){
		printf("An error occurred in the server\n");
	}else if(strcmp(token, "PTR") == 0){
		token = bestStrtok(NULL, " ");

		if(strcmp(token, "DUP") == 0){
			printf("A topic with that name already exists\n");
		}else if(strcmp(token, "FUL") == 0){
			printf("Maximum number of topics reached\n");
		}else if(strcmp(token, "OK") == 0){
			printf("Topic created successfully\n");
		}else{
			printf("Couldn't create topic\n");
		}
	}else{
		printf("Unknown message received from server\n");
	}
}

void f_LQU(char *command, int size){
	if(strtok(NULL, " ") != NULL){
		printf("List questions takes no additional argument\n");
		return;
	}else if(strlen(topic) == 0){
		printf("Select a topic first\n");
		return;
	}

	int n = snprintf(command, size, "LQU %s\n", topic);

	if(UDPcommunication(command, size, n) == -1){
		return;
	}

	char buffer[BUFLEN];
	char *token = bestStrtok(command, " ");

	if(strcmp(token, "ERR") == 0){
		printf("An error occurred on the server\n");
	}else if(strcmp(token, "LQR") == 0){
		token = bestStrtok(NULL, " ");
		int questions = strtol(token, NULL, 10);

		snprintf(buffer, BUFLEN, "%s/%s", TOPICS_FOLDER, topic);

		DIR *dir = opendir(buffer);
		if(!dir){
			if(errno == ENOENT){
				if(mkdir(buffer, 0777) != 0){
					perror("mkdir");
					exit(1);
				}
			}else{
				perror("opendir");
				exit(1);
			}
		}

		closedir(dir);

		snprintf(buffer, BUFLEN, "%s/%s/%s", TOPICS_FOLDER, topic, QUESTION_LIST);

		// Always overwrite file
		FILE *fp = fopen(buffer, "w");
		if(fp == NULL){
			perror("fopen");
			exit(1);
		}

		if(questions == 0){
			printf("No questions available\n");
			return;
		}

		int i = 1;

		printf("QUESTIONS:\n");

		char *uPtr, *aPtr;
		while(i <= questions){
			token = bestStrtok(NULL, " ");
			fprintf(fp, "%s\n", token);

			int colon = strcspn(token, ":");
			token[colon] = '\0';

			uPtr = &(token[colon+1]);

			colon = strcspn(uPtr, ":");
			uPtr[colon] = '\0';

			aPtr = &(uPtr[colon+1]);

			printf("%2d - %s by %s with %s answers\n", i, token, uPtr, aPtr);
			i++;
		}

		fclose(fp);
	}else{
		printf("Unknown message received from server\n");
	}
}

void f_QGet(char *command, int size){
	char *token = strtok(NULL, " ");

	if(userID == 0){
		printf("Register user first\n");
		return;
	}else if(strlen(topic) == 0){
		printf("Select a topic first\n");
		return;
	}else if(token == NULL){
		printf("Question title needed\n");
		return;
	}else if(strlen(token) > 10){
		printf("Question title is at most 10 characters long\n");
		return;
	}

	char buffer[BUFLEN];

	snprintf(buffer, BUFLEN, "%s/%s/%s", TOPICS_FOLDER, topic, QUESTION_LIST);

	if(access(buffer, F_OK)){
		printf("No questions available\nSubmit one or check for a list using 'ql'\n");
		return;
	}

	snprintf(question, QUESTMAX, "%s", token);

	int n = snprintf(command, size, "GQU %s %s\n", topic, question);

	TCPconnect();

	TCPwrite(command, n);
	TCPread(command, size);

	if(strcmp(command, "ERR") == 0){
		printf("An error occurred on the server\n");
	}else if(strcmp(command, "QGR") == 0){
		TCPread(command, size); // Discarding userID (already on client side)

		if(strcmp(command, "EOF") == 0){
			printf("No such topic or question\n");
			TCPclose();
			return;
		}else if(strcmp(command, "ERR") == 0){
			printf("Bad formulation of query\n");
			TCPclose();
			return;
		}

		if(TCPread(command, size) != 2){
			printf("Invalid message from server\n");
			TCPclose();
			return;
		}

		char buffer[BUFLEN];
		char *ptr = buffer;

		ptr += snprintf(buffer, BUFLEN, "%s/%s/%s.txt", TOPICS_FOLDER, topic, question);

		// Get question file
		TCPgetFile(buffer, strtol(command, NULL, 10));

		if(TCPread(command, size) != 3){ // Space
			printf("Invalid message from server\n");
			TCPclose();
			return;
		}

		TCPread(command, size);

		// Check for image
		n = strtol(command, NULL, 10);
		if(n == 1){
			ptr -= 3;

			if(TCPread(command, size) != 2){
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}

			snprintf(ptr, 4, "%s", command);

			if(TCPread(command, size) != 2){
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}

			TCPgetFile(buffer, strtol(command, NULL, 10));

			if(TCPread(command, size) != 3){ // Space
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}
		}else if(n != 0){
			printf("Invalid message from server\n");
			TCPclose();
			return;
		}

		// Num of answers
		TCPread(command, size);

		int answers = strtol(command, NULL, 10);

		while(answers--){
			// Answer number
			if(TCPread(command, size) != 2){
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}

			int aNum = strtol(command, NULL, 10);

			snprintf(buffer, BUFLEN, "%s/%s/%s", TOPICS_FOLDER, topic, ANSWER_LIST);

			// Answerer ID
			if(TCPread(command, size) != 2){
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}

			FILE *fp = fopen(buffer, "a+");
			if(fp == NULL){
				perror("fopen");
				exit(1);
			}

			fprintf(fp, "%s_%02d:%s\n", question, aNum, command);
			fclose(fp);

			ptr = buffer;
			ptr += snprintf(buffer, BUFLEN, "%s/%s/%s_%02d.txt", TOPICS_FOLDER, topic, question, aNum);

			// File size
			if(TCPread(command, size) != 2){
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}

			TCPgetFile(buffer, strtol(command, NULL, 10));
			if(TCPread(command, size) != 3){ // Space
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}

			// Check for image
			TCPread(command, size);

			int m = strtol(command, NULL, 10);
			if(m == 1){
				ptr -= 3;

				if(TCPread(command, size) != 2){
					printf("Invalid message from server\n");
					TCPclose();
					return;
				}

				snprintf(ptr, 4, "%s", command);

				if(TCPread(command, size) != 2){
					printf("Invalid message from server\n");
					TCPclose();
					return;
				}

				TCPgetFile(buffer, strtol(command, NULL, 10));
				TCPread(command, size);
			}else if(m != 0){
				printf("Invalid message from server\n");
				TCPclose();
				return;
			}
		}

		printf("Question retrieved successfully\n");
	}else{
		printf("Unknown message received from server\n");
	}

	TCPclose();

}

void f_QG(char *command, int size){
	char *token = strtok(NULL, " ");

	if(userID == 0){
		printf("Register user first\n");
		return;
	}else if(strlen(topic) == 0){
		printf("Select a topic first\n");
		return;
	}else if(token == NULL){
		printf("Question number needed\n");
		return;
	}


	char buffer[BUFLEN];

	snprintf(buffer, BUFLEN, "%s/%s/%s", TOPICS_FOLDER, topic, QUESTION_LIST);

	if(access(buffer, F_OK)){
		printf("No questions available\nSubmit one or check for a list using 'ql'\n");
		return;
	}

	FILE *fp = fopen(buffer, "r");
	if(fp == NULL){
		if(errno == ENOENT){
			printf("Couldn't find file\n");
			return;
		}

		perror("fopen");
		exit(1);
	}

	int n = strtol(token, NULL, 10);

	if(n == 0){
		printf("Unknown question\n");
		return;
	}

	while (fgets(buffer, BUFLEN, fp) != NULL) {
		n--;

		if(n == 0){
			snprintf(command, size, "question_get %s", strtok(buffer, ":"));
			fclose(fp);

			parseInput(command, size);
			return;
		}
	}

	fclose(fp);

	printf("Unknown question\n");
}

void f_QUS(char *command, int size){
	// Get question name
	char *token = strtok(NULL, " ");

	if(userID == 0){
		printf("Register user first\n");
		return;
	}else if(strlen(topic) == 0){
		printf("Select a topic first\n");
		return;
	}else if(strlen(token) > 10){
		printf("Question title must be at most 10 characters long\n");
		return;
	}

	int n;
	char buffer[BUFLEN];
	char sendBuf[BUFLEN];

	if(token == NULL){
		printf("Question title needed\n");
		return;
	}

	snprintf(question, QUESTMAX, "%s", token);

	// Get question file
	token = strtok(NULL, " ");

	if(token == NULL){
		printf("Question file needed\n");
		return;
	}

	snprintf(buffer, BUFLEN, "%s.txt", token);


	FILE *fp = fopen(buffer, "r");
	if(fp == NULL){
		if(errno == ENOENT){
			printf("Couldn't find file\n");
			return;
		}

		perror("fopen");
		exit(1);
	}

	fseek(fp, 0, SEEK_END);

	n = snprintf(sendBuf, BUFLEN, "QUS %d %s %s %d ", userID, topic, question, (int) ftell(fp));
	fclose(fp);

	TCPconnect();

	TCPwrite(sendBuf, n);
	if(TCPsendFile(buffer) == -1){
		TCPclose();
		return;
	}

	// Check for image
	if((token = strtok(NULL, " ")) != NULL){
		fp = fopen(token, "r");
		if(fp == NULL){
			if(errno == ENOENT){
				printf("Couldn't find file\n");
				TCPclose();
				return;
			}

			perror("fopen");
			exit(1);
		}

		fseek(fp, 0, SEEK_END);

		char *extension = token + strcspn(token, ".") + 1;

		if(strlen(extension) != 3){
			printf("Invalid image extension\n");
			TCPclose();
			return;
		}

		n = snprintf(buffer, BUFLEN, " 1 %s %d ", extension, (int) ftell(fp));
		fclose(fp);

		TCPwrite(buffer, n);
		if(TCPsendFile(token) == -1){
			TCPclose();
			return;
		}
		TCPwrite("\n", 1);
	}else{
		TCPwrite(" 0\n", 3);
	}

	TCPread(command, size);

	if(strcmp(command, "ERR") == 0){
		printf("An error occurred on the server\n");
	}else if(strcmp(command, "QUR") == 0){
		TCPread(command, size);

		if(strcmp(command, "DUP") == 0){
			printf("There is already a question with that name\n");
		}else if(strcmp(command, "FUL") == 0){
			printf("Maximum number of questions for this topic reached\n");
		}else if(strcmp(command, "OK") == 0){
			printf("Question submitted successfully\n");
		}else if(strcmp(command, "NOK") == 0){
			printf("Couldn't submit question\n");
		}
	}else{
		printf("Unknown message received from server\n");
	}

	TCPclose();
}

void f_ANS(char *command, int size){
	char *token = strtok(NULL, " ");

	if(userID == 0){
		printf("Register user first\n");
	}else if(strlen(topic) == 0){
		printf("Select a topic first\n");
		return;
	}else if(strlen(question) == 0){
		printf("Select a question first\n");
		return;
	}else if(token == NULL){
		printf("Answer file needed\n");
		return;
	}

	int n;
	char buffer[BUFLEN];
	char sendBuf[BUFLEN];

	// Get name of file with answer
	snprintf(buffer, BUFLEN, "%s.txt", token);

	FILE *fp = fopen(buffer, "r");
	if(fp == NULL){
		if(errno == ENOENT){
			printf("Couldn't find file\n");
			return;
		}

		perror("fopen");
		exit(1);
	}

	fseek(fp, 0, SEEK_END);

	n = snprintf(sendBuf, BUFLEN, "ANS %d %s %s %d ", userID, topic, question, (int) ftell(fp));
	fclose(fp);

	TCPconnect();

	TCPwrite(sendBuf, n);
	if(TCPsendFile(buffer) == -1){
		TCPclose();
		return;
	}

	// Check for image
	if((token = strtok(NULL, " ")) != NULL){
		fp = fopen(token, "r");
		if(fp == NULL){
			if(errno == ENOENT){
				printf("Couldn't find file\n");
				TCPclose();
				return;
			}

			perror("fopen");
			exit(1);
		}

		fseek(fp, 0, SEEK_END);

		char *extension = token + strcspn(token, ".") + 1;

		if(strlen(extension) != 3){
			printf("Invalid image extension\n");
			TCPclose();
			return;
		}

		n = snprintf(buffer, BUFLEN, " 1 %s %d ", extension, (int) ftell(fp));
		fclose(fp);

		TCPwrite(buffer, n);
		if(TCPsendFile(token) == -1){
			TCPclose();
			return;
		}
		TCPwrite("\n", 1);
	}else{
		TCPwrite(" 0\n", 3);
	}

	TCPread(command, size);

	if(strcmp(command, "ERR") == 0){
		printf("An error occurred on the server\n");
	}else if(strcmp(command, "ANR") == 0){
		TCPread(command, size);

		if(strcmp(command, "FUL") == 0){
			printf("Maximum number of answers for this question reached\n");
		}else if(strcmp(command, "OK") == 0){
			printf("Answer submitted successfully\n");
		}else{
			printf("Couldn't submit answer\n");
		}
	}else{
		printf("Unknown message received from server\n");
	}

	TCPclose();
}
