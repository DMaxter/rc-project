#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <netdb.h>
#include <getopt.h>
#include <errno.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "commands.h"

void handleSignal(int sig){
	int status;
	pid_t PID;

	while((PID = waitpid(-1, &status, WNOHANG)) > 0){
	}

	if(PID == -1){
		// ECHILD is not a problem (it is the stop case of the while loop)
		if(errno == ECHILD){
			errno = 0;
		}else{
			perror("waitpid");
			exit(1);
		}
	}
}

int main(int argc, char* argv[]){
	char *FSPort = malloc(sizeof(char) * 10);
	int opt;

	// ECHILD handling
	struct sigaction signal;

	sigemptyset(&signal.sa_mask);
	signal.sa_flags = SA_NOCLDSTOP | SA_RESTART;
	signal.sa_handler = &handleSignal;

	if(sigaction(SIGCHLD, &signal, NULL) < 0){
		perror("sigaction");
		exit(1);
	}


	strcpy(FSPort, PORT);

	while((opt = getopt(argc, argv, "p:")) != -1){
		switch(opt){
			case 'p': free(FSPort);
					  FSPort = optarg;
					  break;
			default:  printf("Invalid option '-%c'\n", (char) opt);
		}
	}

	struct stat dir;

	// Check for 'topics' folder
	if(stat(TOPICS_FOLDER, &dir) != 0 && !S_ISDIR(dir.st_mode)){
		if(mkdir(TOPICS_FOLDER, 0777) != 0){
			perror("mkdir");
			exit(1);
		}
	}
	// Change to topics folder
	if(chdir(TOPICS_FOLDER) != 0){
		perror("chdir");
		exit(1);
	}


	// File Descriptors
	int tcpFD, udpFD;
	// Mask for select
	fd_set readfd;

	socklen_t addrlen;
	ssize_t err;
	struct sockaddr_in *addr;
	struct addrinfo tcpSock, udpSock, *resTCP, *resUDP;

	// Close stdin fd
	close(fileno(stdin));

	// Clean memory allocated for sockets
	memset(&tcpSock, 0, sizeof tcpSock);
	memset(&udpSock, 0, sizeof udpSock);

	// Socket definition
	tcpSock.ai_family = udpSock.ai_family = AF_INET; // IPV4
	tcpSock.ai_socktype = SOCK_STREAM;
	udpSock.ai_socktype = SOCK_DGRAM;
	tcpSock.ai_flags = udpSock.ai_flags = AI_NUMERICSERV | AI_PASSIVE;

	// TCP Connection
	err = getaddrinfo(NULL, FSPort, &tcpSock, &resTCP);
	if(err != 0){
		fprintf(stderr, "%s", gai_strerror(err));
		exit(1);
	}

	tcpFD = socket(resTCP->ai_family, resTCP->ai_socktype, resTCP->ai_protocol);
	if(tcpFD == -1){
		perror("socket TCP");
		exit(1);
	}

	err = bind(tcpFD, resTCP->ai_addr, resTCP->ai_addrlen);
	if(err == -1){
		perror("bind");
		exit(1);
	}

	err = listen(tcpFD, 5);
	if(err == -1){
		perror("listen");
		exit(1);
	}

	// UDP Connection
	err = getaddrinfo(NULL, FSPort, &udpSock, &resUDP);
	if(err != 0){
		fprintf(stderr, "%s", gai_strerror(err));
		exit(1);
	}

	udpFD = socket(resUDP->ai_family, resUDP->ai_socktype, resUDP->ai_protocol);
	if(udpFD == -1){
		perror("socket UDP");
		exit(1);
	}

	err = bind(udpFD, resUDP->ai_addr, resUDP->ai_addrlen);
	if(err == -1){
		perror("bind UDP");
		exit(1);
	}

	// Wait for connections
	do{
		FD_ZERO(&readfd);
		FD_SET(tcpFD, &readfd);
		FD_SET(udpFD, &readfd);

		if(select(MAX(udpFD, tcpFD) + 1, &readfd, NULL, NULL, NULL) == -1){
			if(errno == EINTR){
				continue;
			}
			perror("select");
			exit(1);
		}

		pid_t pid =	fork();
		if(pid == -1){
			perror("fork");
			exit(1);
		}else if(pid == 0){
			if(FD_ISSET(tcpFD, &readfd)){
				close(udpFD);

				int newfd = accept(tcpFD, (struct sockaddr *) &addr, &addrlen);
				close(tcpFD);
				if(newfd == -1){
					perror("accept");
					exit(1);
				}

				char ip[INET_ADDRSTRLEN];
				struct sockaddr_in* pAddr = (struct sockaddr_in*)&addr;
				struct in_addr ipAddr = pAddr->sin_addr;

				inet_ntop(AF_INET, &ipAddr, &ip, INET_ADDRSTRLEN);

				printf("%s: ", ip);

				TCP(newfd);

				close(newfd);
			}else{
				close(tcpFD);

				UDP(udpFD);
				
				close(udpFD);
			}

			exit(0);
		}
	}while(1);

	freeaddrinfo(resUDP);
	freeaddrinfo(resTCP);
	close(tcpFD);
	close(udpFD);

	return 0;
}
