#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "commands.h"

#define MAX(a,b) ((a>b)?a:b)

char *FSIP, *FSPort;
int UDPfd;
struct addrinfo *resTCP, *resUDP, tcpSock;

int main(int argc, char* argv[]){
	// Check arguments
	parseArgs(argc, argv);

	ssize_t err;
	struct addrinfo udpSock;

	// Clean memory allocated for sockets
	memset(&tcpSock, 0, sizeof tcpSock);
	memset(&udpSock, 0, sizeof udpSock);

	// Socket definition
	tcpSock.ai_family = udpSock.ai_family = AF_INET; // IPV4
	tcpSock.ai_socktype = SOCK_STREAM;
	udpSock.ai_socktype = SOCK_DGRAM;
	tcpSock.ai_flags = udpSock.ai_flags = AI_NUMERICSERV;

	

	// UDP Connection Setup
	err = getaddrinfo(FSIP, FSPort, &udpSock, &resUDP);
	if(err != 0){
		fprintf(stderr, "%s", gai_strerror(err));
		exit(1);
	}

	UDPfd = socket(resUDP->ai_family, resUDP->ai_socktype, resUDP->ai_protocol);
	if(UDPfd == -1){
		perror(NULL);
		exit(1);
	}

	struct stat dir;

	// Check for 'topics' folder
	if(stat(TOPICS_FOLDER, &dir) != 0 && !S_ISDIR(dir.st_mode)){
		if(mkdir(TOPICS_FOLDER, 0777) != 0){
			perror("mkdir");
			exit(1);
		}
	}

	char *command = NULL;
	int len;

	while(1){
		getCommand(&command, &len);
		parseInput(command, len);
	}

	return 0;
}
