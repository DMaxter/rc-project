SOURCES-CLIENT = client/user.c client/commands.c
SOURCES-SERVER = server/FS.c server/commands.c

OBJS-CLIENT = $(SOURCES-CLIENT:%.c=%.o)
OBJS-SERVER = $(SOURCES-SERVER:%.c=%.o)
OBJS = $(OBJS-CLIENT) $(OBJS-SERVER)

CC = gcc
FLAGS = -Wall -Wextra -ggdb3
LDFLAGS = -lm

TARGET-CLIENT = user
TARGET-SERVER = FS

all: $(TARGET-CLIENT) $(TARGET-SERVER)

zip:
	rm proj_21.zip
	zip -r proj_21.zip . -x *.git* *.pdf *.o *.vim

clean:
	@echo Cleaning...
	rm -f $(OBJS) $(TARGET-CLIENT) $(TARGET-SERVER)

objclean:
	@echo Cleaning...
	rm -f $(OBJS)

$(TARGET-CLIENT): $(OBJS-CLIENT)
	$(CC) $(FLAGS) $^ -o $(TARGET-CLIENT) $(LDFLAGS)

$(TARGET-SERVER): $(OBJS-SERVER)
	$(CC) $(FLAGS) $^ -o $(TARGET-SERVER) $(LDFLAGS)

client/user.o: client/user.c client/commands.h
client/commands.o: client/commands.c
server/FS.o: server/FS.c server/commands.h
server/commands.o: server/commands.c

$(OBJS):
	$(CC) $(FLAGS) -c -o $@ $<
